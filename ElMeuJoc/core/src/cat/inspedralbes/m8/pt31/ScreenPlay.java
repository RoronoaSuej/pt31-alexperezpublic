package cat.inspedralbes.m8.pt31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class ScreenPlay implements Screen{
	
	ElMeuJoc joc;
	private BitmapFont nomJoc;
	private BitmapFont nomCreador;
	private BitmapFont press;
	private BitmapFont pressI;
	private Texture InstructionsTexture;
	private Texture PlayTexture;
	
	public ScreenPlay(ElMeuJoc joc) {
		this.joc=joc;
		
		PlayTexture = new Texture(Gdx.files.internal("next.png"));
		InstructionsTexture = new Texture(Gdx.files.internal("I.png"));
		
		nomJoc = new BitmapFont();
		nomJoc.setColor(Color.CYAN);
		nomJoc.getData().setScale(10, 10);
		
		nomCreador = new BitmapFont();
		nomCreador.setColor(Color.CYAN);
		nomCreador.getData().setScale(1, 1);
		
		press = new BitmapFont();
		press.setColor(Color.CYAN);
		press.getData().setScale(3,3);		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		//1.Gestio Input
		gestioInput();
		
		//2.Dibuix
		dibuixa();
	}

	private void dibuixa() {
		joc.batch.begin();
		nomJoc.draw(joc.batch,"Pong",190,530);
		nomCreador.draw(joc.batch,"By Alex Perez",300,50);
		press.draw(joc.batch,"Jugar",300,200);
		press.draw(joc.batch,"Instruccions",300,300);
		joc.batch.draw(InstructionsTexture,200,250,60,60);
		joc.batch.draw(PlayTexture,200,150,60,60);
		joc.batch.end();
	}

	private void gestioInput() {
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			joc.setScreen(new ScreenGame(joc));
		}
		if(Gdx.input.isKeyPressed(Keys.I)) {
			joc.setScreen(new ScreenInfo(joc));
		}
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		nomJoc.dispose();
		nomCreador.dispose();
		press.dispose();
		pressI.dispose();
		InstructionsTexture.dispose();
		PlayTexture.dispose();
		
	}

}
