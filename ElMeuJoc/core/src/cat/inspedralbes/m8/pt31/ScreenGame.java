package cat.inspedralbes.m8.pt31;

import java.text.DecimalFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

public class ScreenGame implements Screen{

	ElMeuJoc joc;
	
	private Texture player1Texture;
	private Texture player2Texture;
	private Texture limit;
	private Texture ballTexture;
	
	private Rectangle limitSuperior;
	private Rectangle limitInferior;
	private Rectangle player1;
	private Rectangle player2;
	private Circle ball;
	
	private DecimalFormat df;
	
	private Sound laser;
	private Sound paret;
	
	private BitmapFont marcadors;
	private BitmapFont velocitatfont;
	
	private float velocitatJugadors = 300;
	private int marcadorJugador1;
	private int marcadorJugador2;
	private float velocitatPilotaX;
	private float velocitatPilotaY;
	private float velocitat;
	private float direccioX;
	private float direccioY;
	
	private float delta;
	
	public ScreenGame(ElMeuJoc joc) {
		this.joc=joc;
		marcadorJugador1=0;
		marcadorJugador2=0;
		
		df = new DecimalFormat("#.00");
		player1 = new Rectangle(0,240, 28, 119);
		player2 = new Rectangle(672,240, 28, 119);
		limitSuperior = new Rectangle(0,590, 700, 10);
		limitInferior = new Rectangle(0,0, 700, 10);
		ball = new Circle(345,295, 10);
		
		limit = new Texture(Gdx.files.internal("cyan.jpg"));
		player1Texture = new Texture(Gdx.files.internal("player1.png"));
		player2Texture = new Texture(Gdx.files.internal("player2.png"));
		ballTexture = new Texture(Gdx.files.internal("pilota.png"));
		
		laser = Gdx.audio.newSound(Gdx.files.internal("laser.mp3"));
		paret = Gdx.audio.newSound(Gdx.files.internal("paret.mp3"));
		
		resetRonda();
		
		actualitzaVelocitat();
		
		marcadors = new BitmapFont();
		marcadors.setColor(Color.CYAN);
		marcadors.getData().setScale(2, 2);
		
		velocitatfont = new BitmapFont();
		velocitatfont.setColor(Color.CYAN);
		velocitatfont.getData().setScale(1, 1);
	}
	
	private void actualitzaVelocitat() {
		velocitat = (float) Math.sqrt(Math.pow(velocitatPilotaX,2)+Math.pow(velocitatPilotaY,2));
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		//1.Gestio input
		gestioInput();
		
		//2.CalculsPilota
		actualitza();
		
		//3.Dibuix
		dibuixa();
		
		//4.ComprovaWin
		comprovaVictoria();	
	}
	private void dibuixa() {
		joc.batch.begin();
		for(int i=0;i<700;i++) {
			marcadors.draw(joc.batch,"I",350,i);
		}
		
		marcadors.draw(joc.batch,Integer.toString(marcadorJugador1),165,565);
		marcadors.draw(joc.batch,Integer.toString(marcadorJugador2),515,565);
		velocitatfont.draw(joc.batch,"Velocitat: "+df.format(velocitat)+"Px/s",100,50);
		joc.batch.draw(player1Texture,player1.x,player1.y,player1.width,player1.height);
		joc.batch.draw(player2Texture,player2.x,player2.y,player2.width,player2.height);
		joc.batch.draw(limit,limitSuperior.x,limitSuperior.y,limitSuperior.width,limitSuperior.height);
		joc.batch.draw(limit,limitInferior.x,limitInferior.y,limitInferior.width,limitInferior.height);
		joc.batch.draw(ballTexture,ball.x,ball.y,10,10);
		joc.batch.end();	
	}

	private void comprovaVictoria() {
		if(marcadorJugador1==10) {
			joc.setScreen(new ScreenWin(joc,1));
		}
		if(marcadorJugador2==10) {
			joc.setScreen(new ScreenWin(joc,2));
		}
		
	}

	private void actualitza() {
		delta=Gdx.graphics.getDeltaTime();
		ball.x += velocitatPilotaX*delta;
		ball.y += velocitatPilotaY*delta;
		
		if(Intersector.overlaps(ball,limitInferior)) {
			paret.play(1.0f);
			velocitatPilotaY=Math.round(-velocitatPilotaY*1.05f);
			actualitzaVelocitat();
		}
		if(Intersector.overlaps(ball,limitSuperior)) {
			paret.play(1.0f);
			velocitatPilotaY=Math.round(-velocitatPilotaY*1.05f);
			actualitzaVelocitat();
		}
		if(Intersector.overlaps(ball,player1)) {
			laser.play(1.0f);
			velocitatPilotaX=Math.round(-velocitatPilotaX*1.05f);
			actualitzaVelocitat();
		}
		if(Intersector.overlaps(ball,player2)) {
			laser.play(1.0f);
			velocitatPilotaX=Math.round(-velocitatPilotaX*1.05f);
			actualitzaVelocitat();
			
		}
		if(ball.x<28) {
			marcadorJugador2+=1;
			resetRonda();
		}
		if(ball.x>672) {
			marcadorJugador1+=1;
			resetRonda();
		}
	}

	private void resetRonda() {
		direccioX = Math.round(Math.random()*2+1);
		direccioY = Math.round(Math.random()*2+1);
		velocitatPilotaX = (float) (Math.random()*200+150);
		velocitatPilotaY = (float) (Math.random()*200+150);
		if(direccioX==1) {
			velocitatPilotaX=-velocitatPilotaX;
		}
		if(direccioY==1) {
			velocitatPilotaY=-velocitatPilotaY;
		}
		ball.x=345;
		ball.y=295;
		player1.x=0;
		player1.y=240;
		player2.x=672;
		player2.y=260;
		
		actualitzaVelocitat();
		
	}

	private void gestioInput() {
		int altpantalla = Gdx.graphics.getHeight();
		delta=Gdx.graphics.getDeltaTime();
		
		if(Gdx.input.isKeyPressed(Keys.W)) {
			player1.y += velocitatJugadors*delta;
			if(player1.y>altpantalla-player1.height) {
				player1.y=altpantalla-player1.height;
			}
		}
		if(Gdx.input.isKeyPressed(Keys.S)) {
			player1.y -= velocitatJugadors*delta;
			if(player1.y<0) {
				player1.y=0;
			}
		}
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			player2.y += velocitatJugadors*delta;
			if(player2.y>altpantalla-player2.height) {
				player2.y=altpantalla-player2.height;
			}
		}
		if(Gdx.input.isKeyPressed(Keys.DOWN)) {
			player2.y -= velocitatJugadors*delta;
			if(player2.y<0) {
				player2.y=0;
			}
		}
	}


	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		 player1Texture.dispose();
		 player2Texture.dispose();
		 limit.dispose();
		 ballTexture.dispose();
		
		 laser.dispose();
		 paret.dispose();
		
		 marcadors.dispose();
		 velocitatfont.dispose();
		
	}

}
