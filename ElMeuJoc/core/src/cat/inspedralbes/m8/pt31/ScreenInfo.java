package cat.inspedralbes.m8.pt31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class ScreenInfo implements Screen{
	
	ElMeuJoc joc;

	private BitmapFont textJugador;
	private BitmapFont textFuncioTecla;
	private BitmapFont textTornar;
	
	private Texture backTexture;
	private Texture upTexture;
	private Texture downTexture;
	private Texture wTexture;
	private Texture sTexture;
	
	
	
	public ScreenInfo(ElMeuJoc joc) {
		this.joc=joc;
		
		backTexture = new Texture(Gdx.files.internal("back.png"));
		upTexture = new Texture(Gdx.files.internal("amunt.png"));
		downTexture = new Texture(Gdx.files.internal("avall.png"));
		wTexture = new Texture(Gdx.files.internal("w.png"));
		sTexture = new Texture(Gdx.files.internal("s.png"));
		
		textJugador = new BitmapFont();
		textJugador.setColor(Color.CYAN);
		textJugador.getData().setScale(3, 3);
		
		textFuncioTecla = new BitmapFont();
		textFuncioTecla.setColor(Color.CYAN);
		textFuncioTecla.getData().setScale(2, 2);
		
		textTornar = new BitmapFont();
		textTornar.setColor(Color.CYAN);
		textTornar.getData().setScale(3,3);	
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		//1.Gestio Input
		gestioInput();
		
		//2.Dibuix
		dibuixa();
	}

	private void dibuixa() {
		joc.batch.begin();
		textJugador.draw(joc.batch,"Jugador 1",100,300);
		textJugador.draw(joc.batch,"Jugador 2",400,300);
		textFuncioTecla.draw(joc.batch,"Amunt",200,500);
		textFuncioTecla.draw(joc.batch,"Avall",200,400);
		textFuncioTecla.draw(joc.batch,"Amunt",500,500);
		textFuncioTecla.draw(joc.batch,"Avall",500,400);
		textTornar.draw(joc.batch,"Tornar",300,100);
		joc.batch.draw(backTexture,200,50,60,60);
		joc.batch.draw(upTexture,100,450,60,60);
		joc.batch.draw(downTexture,100,350,60,60);
		joc.batch.draw(wTexture,400,450,60,60);
		joc.batch.draw(sTexture,400,350,60,60);
		joc.batch.end();
	}
	private void gestioInput() {
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			joc.setScreen(new ScreenPlay(joc));
		}	
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		textJugador.dispose();
		textFuncioTecla.dispose();
		textTornar.dispose();
		backTexture.dispose();
		upTexture.dispose();
		downTexture.dispose();
		wTexture.dispose();
		sTexture.dispose();
		
	}

}
