package cat.inspedralbes.m8.pt31;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class ScreenWin implements Screen{
	
	ElMeuJoc joc;
	
	private Texture playAgain;
	
	private BitmapFont guanyadorText;
	private BitmapFont tornaraJugar;
	
	private String text;
	
	private Sound win;
	
	public ScreenWin(ElMeuJoc joc,int guanyador) {
		this.joc=joc;
		
		playAgain = new Texture(Gdx.files.internal("next.png"));
		
		guanyadorText = new BitmapFont();
		guanyadorText.setColor(Color.CYAN);
		guanyadorText.getData().setScale(2, 2);
		
		tornaraJugar = new BitmapFont();
		tornaraJugar.setColor(Color.CYAN);
		tornaraJugar.getData().setScale(1, 1);
		
		if(guanyador==1) {
			text="Ha guanyat el jugador 1";
		}else {
			text="Ha guanyat el jugador 2";
		}
		win = Gdx.audio.newSound(Gdx.files.internal("victory.mp3"));
		win.play(1.00f);
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		//1.Gestio Input
		gestioInput();
		
		//2.Dibuix
		dibuixa();
		
	}

	private void dibuixa() {
		joc.batch.begin();
		guanyadorText.draw(joc.batch,text,200,450);
		tornaraJugar.draw(joc.batch,"Tornar a jugar?",250,150);
		joc.batch.draw(playAgain,350,120,60,60);
		joc.batch.end();
	}

	private void gestioInput() {
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			joc.setScreen(new ScreenGame(joc));
		}	
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		playAgain.dispose();		
		guanyadorText.dispose();
		tornaraJugar.dispose();			
		win.dispose();
		
	}

}
