package cat.inspedralbes.m8.pt31;



import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class ElMeuJoc extends Game {
	SpriteBatch batch;

	
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		this.setScreen(new ScreenPlay(this));
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0/255f, 0/255f, 0/255f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
		
	}

	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
